# utilidadesIntellij

## Búsqueda

- Búsqueda global: `Shift + Shift`.
- Buscar acciones: `Ctrl + Shift + A`.
- Buscar clase: `Ctrl + N`.
- Buscar archivos: `Ctrl + Shift + N`.
- Buscar símbolos: `Ctrl + Shift + Alt + N`.
- Buscar usos: `Alt + F7`.
- [Propio] Buscar usos: `Alt + U`.
- Buscar todos los usos de clases o métodos: `Ctrl + Alt + F7`.
- Buscar en el fichero actual: `Ctrl + F`.
- Buscar en todos los ficheros: `Ctrl + Shift + F`.

## Debug

- Siguiente línea: `F8`.
- Entrar en el método: `F7`.
- Salir del método: `Shift + F8`.
- Ir hasta siguiente punto de ruptura: `F9`.
- Evaluar expresión: `Alt + F8`.
- Relanzar ejecución: `Ctrl + F5`.
- Parar ejecución: `Ctrl + F2`.

## Edición

- Deshacer: `Ctrl + Z`.
- Rehacer: `Ctrl + Shift + Z` ó `Ctrl + Y`.
- Mover líneas: `Shift + Alt + ↑ / ↓`.
- Mover métodos: `Ctrl + Shift + ↑ / ↓`.
- Extender selección: `Ctrl + W`.
- Reducir selección: `Ctrl + Shift + W`.
- Añadir nuevo cursor: `Shift + Alt + Click`.
- Seleccion multicursor: `Alt + Arrastrar selección`.
- Activar/desactivar modo columna: `Shift + Alt + Insert`.
- Multicursor con teclado: `Activar modo columna + Shift + ↑ / ↓`.
- Ir a la siguiente ocurrencia: `Ctrl + L`.
- Ir a la anterior ocurrencia: `Ctrl + Shift + L`.
- Seleccionar siguiente ocurrencia: `Alt + J`.
- Deseleccionar ocurrencia: `Shift + Alt + J`.
- Seleccionar todas las ocurrencias: `Ctrl + Shift + Alt + J`.
- Encontrar uso de una variable en el fichero actual: `Ctrl + Shift + F7`.
- Ir al siguiente uso de la variable: `F3`.
- Ir al anterior uso de la variable: `Shift + F3`.
- Duplicar línea: `Ctrl + D`.
- Eliminar línea: `Ctrl + Y`.
- [Propio] Eliminar línea: `Ctrl + Shift + Y` ó `Ctrl + Shift + D`.
- Eliminar línea actual: `Shift + Supr`.
- Optimizar imports: `Ctrl + Alt + O`.
- Importar clase: `Alt + Intro`.
- Formatear código: `Ctrl + Alt + L`.
- Formatear todos los ficheros de un proyecto: `Sobre la carpeta raíz: Ctrl + Alt + L`.
- Mover clase: `Sobre el nombre de la clase: F6`.
- Renombrar clase, método o variable: `Shift + F6`.
- Introducir constante: `Sobre una variable dentro de un método: Ctrl + Alt + C`.
- Introducir campo: `Sobre una variable dentro de un método: Ctrl + Alt + F`.
- Introducir parámtero: `Sobre una variable dentro de un método: Ctrl + Alt + P`.
- Reemplazar en el fichero actual: `Ctrl + R`.
- Reemplazar en todos los ficheros: `Ctrl + Shift + R`.
- Autocompletar tipo de cierre: `Ctrl + Shift + Intro`.
- Añadir línea en blanco por encima: `Ctrl + Alt + Intro`.
- Alternar case: `Shift + Alt + U`.
- [Propio] Crear carpeta: `Alt + P`.
- [Propio] Crear fichero Java: `Alt + J`.
- [Propio] Crear fichero: `Alt + F`.
- Comentar línea: `Ctrl + / (Numpad)`.
- [Propio] Comentar línea: `Ctrl + Ç`.
- Comentar bloque: `Ctrl + Shift + / (Numpad)`.
- [Propio] Comentar bloque: `Ctrl + Shift + Ç`.
- Extraer a variable: `Ctrl + Alt + V`.
- Extraer a método: `Ctrl + Alt + M`.
- Envolver con: `Ctrl + Alt + T`.

## GIT

- Hacer pull: `Ctrl + T`.
- Crear commit: `Ctrl + K`.
- Hacer push: `Ctrl + Shift + K`.
- [Propio] Ver cambios de un fichero en el histórico: `Alt + H`.
- [Propio] Ver autor de línea: `Alt + A`.

## Menús

- Mostrar menús laterales: `Alt + Alt (sostenido)`.
- Abrir opciones: `Ctrl + Alt + S`.
- Ejecutar comando: `Ctrl + Ctrl`.
- Cambios por subir: `Alt + 0`.
- Vista de proyecto: `Alt + 1`.
- Favoritos: `Alt + 2`.
- Mostrar búsqueda: `Alt + 3`.
- Ejecutar: `Alt + 4`.
- Debug: `Alt + 5`.
- Problemas: `Alt + 6`.
- Estructura de la clase: `Alt + 7`.
- Servicios: `Alt + 8`.
- Git: `Alt + 9`.
- Terminal: `Alt + F12`.
- [Propio] Mostrar en vista de proyecto: `Alt + V`.
- Expandir/Reducir ventana de menú: `Ctrl + Shift + '`.
- Nueva ventana de terminal: `Ctrl + Shift + T`.
- Cerrar ventana de terminal: `Ctrl + Shift + F4`.
- Cerrar última ventana de menú abierta: `Shift + Esc`.
- Generar código: `Alt + Insert`.
- Sobrescribir métodos: `Ctrl + O`.
- Implementar métodos de la interfaz: `Ctrl + I`.
- Histórico portapapeles: `Ctrl + Shift + V`.
- Menú formatear código: `Ctrl + Shift + Alt + L`.
- Abrir menú de acciones: `Sobre avisos: Alt + Intro`.
- Mostrar menú refactor: `Ctrl + Shift + Alt + T`.
- Estructura de proyecto: `Ctrl + Shift + Alt + S`.
- Estructura del proyecto: `Con la ventana de proyecto abierta: F4`.
- [Propio] Branches: `Alt + G`.
- [Propio] Mostrar terminal: `Alt + T`.
- [Propio] Mostrar pestaña base de datos: `Alt + D`.
- [Propio] Mostrar histórico local: `Shift + Alt + H`.
- [Propio] Mostrar histórico local para la selección: `Shift + Alt + S`.
- Mostrar en explorador de archivos: `Sobre la pestaña: Ctrl + Click`.
- Comparar dos archivos o carpetas: `Seleccionados sobre el proyecto: Ctrl + D`.
- Ver jerarquía de una clase: `Ctrl + H`.

## Navegación

- Archivos recientes: `Ctrl + E`.
- Localizaciones recientes: `Ctrl + Shift + E`.
- Mostrar ventanas abiertas: `Ctrl + Tab`.
- Abrir clase en una nueva ventana: `Shift + F4`.
- Cerrar pestaña: `Ctrl + F4`.
- Mostrar página navegación paquetes: `Alt + Inicio`.
- Ver la estructura de archivo: `Ctrl + F12`.
- Ir al inicio o final de unas llaves o paréntesis: `Ctrl + Shift + M`.
- Ir a línea: `Ctrl + G`.
- Ir al test o crear test: `Ctrl + Shift + T`.
- Abrir preview de una clase o método: `Ctrl + Shift + I`.
- Abrir carpeta: `En vista de proyecto: + (Numpad)` ó `En vista de proyecto: Intro`.
- Abrir todas las carpetas: `En vista de proyecto: Ctrl + + (Numpad)`.
- Cerrar carpeta: `En vista de proyecto: - (Numpad)` ó `En vista de proyecto: Intro`.
- Cerrar todas las carpetas: `En vista de proyecto: Ctrl + - (Numpad)`.
- Ir a una clase o método desde una referencia: `Ctrl + B`.
- Ir a la declaración de tipo de un método: `Ctrl + Shift + B`.
- Ir a la implementación: `Ctrl + Alt + B`.
- Ir a la interfaz: `Desde la implementación: Ctrl + U`.
- Ir al anterior método: `Alt + ↑`.
- Ir al siguiente método: `Alt + ↓`.
- Navegar por el historial: `Ctrl + Alt + ← / →`.
- Ir a la anterior o siguiente pestaña: `Alt + ← / →`.
- Abrir en el explorador de archivos: `Ctrl + Alt + F12`.
- Abrir elemento en otra vista: `Alt + F1`.
- Abrir en diagrama UML en ventana emergente: `Ctrl + Alt + U`.
- Abrir en vista de diagrama UML: `Ctrl + Shift + Alt + U`.
- Abrir vista previa: `Con un fichero seleccionado: espacio`.
- Editar fichero: `Con un fichero seleccionado o con la vista previa abierta: F4`.
- Editar fichero: `Con un fichero seleccionado: Intro`.
- [Propio] Mover al inicio del método: `` Ctrl + ` ``.
- [Propio] Mover al inicio del método seleccionando: `` Ctrl + Shift + ` ``.
- [Propio] Mover al final del método: `Ctrl + +`.
- [Propio] Mover al final del método seleccionando: `Ctrl + Shift + +`.

## Varios

- Construir proyecto: `Ctrl + F9`.
- Mostrar información de los parámetros de un método: `Ctrl + P`.
- Mostrar información del tipo: `Ctrl + Shift + P`.
- Cerrar otras pestañas: `Sobre la que se mantendrá abierta: Alt + Click`.
- Refrescar proyecto: `Ctrl + Alt + Y`.
- Ejecutar aplicación: `Shift + F10`.
- Debuggear aplicación: `Shift + F9`.
- Parar servidor: `Ctrl + F2`.
- Arrancar servidor: `Con los servicios abiertos: Ctrl + Shift + F10`.
- [Propio] Debuggear servidor: `Con los servicios abiertos: Shift + Alt + D`.
- Crear nuevo fichero temporal: `Ctrl + Shift + Alt + Insert`.
- Copiar referencia: `Ctrl + Shift + Alt + C`.
- [Propio] Dividir pantalla abajo: `Alt + -`.
- [Propio] Dividir pantalla derecha: `Alt + +`.
- Ejecutar clase completa de test o un método específico: `Ctrl + Shift + F10`.
- Debuggear clase completa de test o un método específico: `Shift + Alt + D`.
- Añadir a favoritos: `F11`.
- Mostrar documentación en ventana emergente: `Ctrl + Q`. Se puede cambiar la forma en la que se muestra volviendo a pulsar `Ctrl + Q` para mostrar la documentación en una pestaña.
- Previsualizar una imagen en ventana emergente: `Ctrl + Shift + I`.
- Unir líneas eliminando espacios innecesarios: `Ctrl + Shift + J`.
- Ver métodos de la interfaz implementada: `Sobre "implements": Ctrl + Shift + F7`.
- Ver métodos que puede lanzar una excepción: `Sobre "throws": Ctrl + Shift + F7`.

## Tips

- [File templates](https://www.jetbrains.com/help/idea/file-template-variables.html).
- Se pueden ocultar las barras de menú en clicando en el icono de la esquina inferior izquierda.
- Se pueden mostrar/ocultar los Breadcrumbs en: Editor -> General -> Breadcrumbs -> Show breadcrumbs.
- Se pueden mostrar/ocultar los espacios en: Editor -> General -> Appearance -> Show whitespaces.
- Se pueden ocultar las pestañas en: Editor -> General -> Editor tabs -> Tab placement: none.
- Para ocultar la barra de navegación, en el menú de acciones buscar "Navigation bar" y desactivarlo.
- Se pueden activar/desactivar las ligaduras de las fuentes en: Editor -> Color Scheme -> Color Scheme Font -> Enable ligatures.
- Se puede cambiar la tipografía, el tamaño de letra y el espacio entre líneas en: Editor -> Color Scheme -> Color Scheme Font -> Enable ligatures.
- Se puede habilitar la opción para generar serialVersionUID para las clases serializables en: Editor -> Inspections -> Serializable class without 'serialVersionUID'.
- Se puede mostrar el nombre de los parámetros en el autocompletado en: Editor -> General -> Code Completion -> Show parameter name hints on completion.
